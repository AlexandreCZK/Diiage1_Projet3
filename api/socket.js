var io = require('socket.io')();

var socketApi = {
    io: io
};

module.exports = socketApi;